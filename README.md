# Pixel Puzzle

[![Pixel Puzzle title image](/img/screenshot_10-15.jpg)](https://pixel-fabian.itch.io/pixel-puzzle)

A logic puzzle where you need to color cells or leave them blank according to the numbers at the side of the grid.

Build with [phaser 3](https://phaser.io/)

## Play

> Play on [itch.io](https://pixel-fabian.itch.io/pixel-puzzle)

### Controls 

- Left click / touch: Color cell
- Right click or hold click / touch: Mark as blank

## Licence

### Code

Pixel Puzzle - A browser game - Copyright (C) 2022 pixel-fabian

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.

### Assets 

- Images: pixel-fabian CC-BY-SA 4.0
- Font Nunito