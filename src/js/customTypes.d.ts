import Field from './objects/Field';

export interface Counts {
  totalFilled: number;
  curFilled: number;
  curErrors: number;
}

export interface SaveGameData {
  size: {
    rows: number;
    columns: number;
  };
  fieldsData?: Array<Array<Object>>;
  counts?: Counts;
}

