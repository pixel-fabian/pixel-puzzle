/* eslint-disable no-unused-vars */
export enum SCENES {
  LOAD = 'LoadScene',
  MENU = 'MenuScene',
  GAME = 'GameScene',
  NEW_GAME = 'NewGameScene',
  UI = 'UiScene',
  CREDITS = 'CreditsScene',
}

export enum TEXTURES {
  LOGO = 'logo',
}

export const COLORS = {
  BACKGROUND: new Phaser.Display.Color(18, 3, 48, 255),
  BACKGROUND_BRIGHTER: new Phaser.Display.Color(43, 28, 73, 255),
  TEXT: new Phaser.Display.Color(255, 255, 255, 255),
  PRIMARY: new Phaser.Display.Color(242, 19, 183, 255),
  PRIMARY_DARKER: new Phaser.Display.Color(102, 47, 101, 255),
  SECONDARY: new Phaser.Display.Color(11, 158, 255, 255),
  ERROR: new Phaser.Display.Color(255, 0, 0, 255),
};

export enum EVENTS {
  UPDATE_PROGRESS = 'updateProgress',
  UPDATE_ERRORS = 'updateError',
  GAMEOVER = 'gameover',
}
