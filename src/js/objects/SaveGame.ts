import { SaveGameData } from '../customTypes';
import Field from './Field';

export default class SaveGame {
  private saveGameVersion = '4';
  private oData: SaveGameData = {
    size: {
      rows: 0,
      columns: 0,
    },
    fieldsData: [],
    counts: { totalFilled: 0, curFilled: 0, curErrors: 0 },
  };

  constructor() {
    this._loadFromLS();
  }

  setData(oData) {
    if (oData.size) {
      this.oData.size = oData.size;
    }
    if (oData.fields) {
      this._setFieldsData(oData.fields);
    }
    if (oData.counts) {
      this.oData.counts = oData.counts;
    }
    this._saveToLS();
  }

  getData() {
    this._loadFromLS();
    return this.oData;
  }

  hasData() {
    this._loadFromLS();
    if (this.oData.fieldsData.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  clearData() {
    this.oData = {
      size: {
        rows: 0,
        columns: 0,
      },
      fieldsData: [],
      counts: { totalFilled: 0, curFilled: 0, curErrors: 0 },
    };
    this._removeFromLS();
  }

  /////////////////////////////////////////////////
  // Interact with local storage                 //
  /////////////////////////////////////////////////

  private _saveToLS() {
    if (this.isLocalStorageAvailable()) {
      window.localStorage.setItem('puzzle', JSON.stringify(this.oData));
      window.localStorage.setItem('saveGameVersion', this.saveGameVersion);
    }
  }

  private _loadFromLS() {
    if (
      this.isLocalStorageAvailable() &&
      JSON.parse(window.localStorage.getItem('puzzle')) &&
      this.saveGameVersion == window.localStorage.getItem('saveGameVersion')
    ) {
      this.oData = JSON.parse(window.localStorage.getItem('puzzle'));
    }
  }

  private _removeFromLS() {
    if (
      this.isLocalStorageAvailable() &&
      JSON.parse(window.localStorage.getItem('puzzle'))
    ) {
      window.localStorage.removeItem('puzzle');
    }
  }

  //////////////////////////////////////////////////
  // Private methods                              //
  //////////////////////////////////////////////////

  _setFieldsData(fields: Array<Array<Field>>) {
    const fieldsData = [];

    if (fields.length) {
      for (let row = 0; row < fields.length; row++) {
        fieldsData.push([]);
        for (let column = 0; column < fields[row].length; column++) {
          const fieldData = {
            filled: fields[row][column].getFilled(),
            solved: fields[row][column].getSolved(),
            error: fields[row][column].getError(),
          };
          fieldsData[row].push(fieldData);
        }
      }
      this.oData.fieldsData = fieldsData;
    }
  }

  private isLocalStorageAvailable() {
    try {
      const key = `__storage__test`;
      window.localStorage.setItem(key, null);
      window.localStorage.removeItem(key);
      return true;
    } catch (e) {
      return false;
    }
  }
}
