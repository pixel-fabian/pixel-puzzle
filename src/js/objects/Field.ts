import { COLORS } from '../constants';
import GameScene from '../scenes/GameScene';
import Utils from '../utils';

export default class Field extends Phaser.GameObjects.Rectangle {
  private colorDefault: number = Utils.colorToHex(COLORS.BACKGROUND);
  private colorBorder: number = Utils.colorToHex(COLORS.PRIMARY);
  private colorHover: number = Utils.colorToHex(COLORS.BACKGROUND_BRIGHTER);
  private colorFilled: number = Utils.colorToHex(COLORS.SECONDARY);
  private colorError: string = Utils.colorToString(COLORS.PRIMARY);
  private colorEmpty: number = Utils.colorToHex(COLORS.PRIMARY_DARKER);
  private filled = false;
  private solved = false;
  private error = false;
  private isPressed = false;
  public scene: GameScene;

  constructor(
    scene: GameScene,
    x: number,
    y: number,
    size: number,
    rndFilled: boolean,
  ) {
    super(scene, x, y, size, size);
    scene.add.existing(this);

    this.setOrigin(0)
      .setFillStyle(this.colorDefault)
      .setStrokeStyle(1, this.colorBorder)
      .setInteractive({ useHandCursor: true });
    this._addEventHandler();
    if (rndFilled) {
      this._setRndFilled();
    }
  }

  onPressFieldSolveFilled() {
    if (this.solved || this.error) return;
    if (this.filled) {
      this._fillWithEffect(this.colorFilled);
      this.solved = true;
      this.scene.addFilled();
    } else {
      this.setFillStyle(this.colorEmpty);
      this._setErrorEffect();
      this._setErrorText(this.x, this.y);
      this.error = true;
      this.scene.addError();
    }
    this.scene.saveData();
  }

  onPressFieldSolveEmpty() {
    if (this.solved || this.error) return;
    if (!this.filled) {
      this._fillWithEffect(this.colorEmpty);
      this.solved = true;
    } else {
      this.setFillStyle(this.colorFilled);
      this._setErrorEffect();
      this._setErrorText(this.x, this.y);
      this.error = true;
      this.scene.addFilled();
      this.scene.addError();
    }
    this.scene.saveData();
  }

  _addEventHandler() {
    this.on('pointerover', () => {
      if (!this.solved && !this.error) {
        this.setFillStyle(this.colorHover);
      }
    });
    this.on('pointerout', () => {
      if (!this.solved && !this.error) {
        this.setFillStyle(this.colorDefault);
        this.isPressed = false;
      }
    });
    this.on('pointerdown', (pointer: Phaser.Input.Pointer) => {
      if (this.scene.gameOver) return;
      this.isPressed = true;

      if (pointer.rightButtonDown()) {
        this.onPressFieldSolveEmpty();
      } else if (pointer.leftButtonDown() || pointer.wasTouch) {
        // check if field is still pressed after 2 seconds
        this.scene.time.addEvent({
          delay: 900,
          callback: () => {
            if (this.isPressed) {
              this.onPressFieldSolveEmpty();
              this.isPressed = false;
            }
          },
        });
      }
    });
    this.on('pointerup', (pointer: Phaser.Input.Pointer) => {
      if (this.scene.gameOver || !this.isPressed) return;

      if (pointer.leftButtonReleased()) {
        this.onPressFieldSolveFilled();
      }
      this.isPressed = false;
    });
  }

  _setRndFilled() {
    const rndNumber = Phaser.Math.Between(0, 1);

    if (rndNumber == 1) {
      this.filled = true;
    }
  }

  _fillWithEffect(color: number) {
    const tmpGroup = this.scene.add.group();
    for (let index = 0; index < 4; index++) {
      this.scene.time.addEvent({
        delay: index * 100,
        callback: () => {
          const tmpRectSize = Phaser.Math.Between(
            this.width / 20,
            this.width / 12,
          );
          const tmpX = this.x + this.width / 2;
          const tmpY = this.y + this.height / 2;
          const tmpRectAngle = Phaser.Math.Between(-180, 180);
          const tmpRect = this.scene.add
            .rectangle(
              Phaser.Math.Between(tmpX - 5, tmpX + 5),
              Phaser.Math.Between(tmpY - 5, tmpY + 5),
              tmpRectSize,
              tmpRectSize,
              color,
            )
            .setAlpha(0)
            .setScale(1)
            .setOrigin(0.5)
            .setAngle(tmpRectAngle);
          tmpGroup.add(tmpRect);
          // animate
          this.scene.tweens.add({
            targets: tmpRect,
            alpha: 0.8,
            scale: 8,
            duration: 200,
          });
        },
        loop: false,
      });
    }

    this.scene.time.addEvent({
      delay: 400,
      callback: () => {
        this.setFillStyle(color);
        tmpGroup.destroy(true, true);
      },
      loop: false,
    });
  }

  _setErrorEffect() {
    this.scene.cameras.main.shake(100, 0.01);
  }

  _setErrorText(x, y) {
    this.scene.add
      .text(x + this.width / 2, y + this.height / 2, 'X', {
        fontFamily: 'Nunito',
        color: this.colorError,
        fontSize: '28px',
      })
      .setOrigin(0.5);
  }

  getFilled() {
    return this.filled;
  }

  setFilled(bool: boolean) {
    this.filled = bool;
  }

  getSolved() {
    return this.solved;
  }

  setSolved(bool: boolean) {
    this.solved = bool;
    // set color
    if (this.solved) {
      if (this.filled) {
        this.setFillStyle(this.colorFilled);
      } else if (!this.filled) {
        this.setFillStyle(this.colorEmpty);
      }
    }
  }

  getError() {
    return this.error;
  }

  setError(bool: boolean) {
    this.error = bool;
    // set color
    if (this.error) {
      if (this.filled) {
        this.setFillStyle(this.colorFilled);
        this._setErrorText(this.x, this.y);
      } else if (!this.filled) {
        this.setFillStyle(this.colorEmpty);
        this._setErrorText(this.x, this.y);
      }
    }
  }

  isEmpty() {
    if (!this.filled && !this.solved && !this.error) {
      return true;
    } else {
      return false;
    }
  }

  changeColorRnd() {
    if (this.filled) {
      const color = new Phaser.Display.Color(
        Phaser.Math.Between(0, 20),
        Phaser.Math.Between(120, 170),
        Phaser.Math.Between(200, 255),
        255,
      );
      this.setFillStyle(color.color);
    } else {
      const color = new Phaser.Display.Color(
        Phaser.Math.Between(90, 150),
        Phaser.Math.Between(20, 60),
        Phaser.Math.Between(90, 150),
        255,
      );
      this.setFillStyle(color.color);
    }
  }
}
