export default class SpriteButton extends Phaser.GameObjects.Sprite {
  constructor(
    scene: Phaser.Scene,
    x: number,
    y: number,
    onPress: Function,
    texture: string | Phaser.Textures.Texture,
    frame?: string | number,
  ) {
    super(scene, x, y, texture, frame);
    scene.add.existing(this);
    this.setInteractive({ useHandCursor: true });

    this.on('pointerdown', () => {
      onPress();
    });
  }
}
