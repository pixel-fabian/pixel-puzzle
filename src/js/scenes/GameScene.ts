/* eslint-disable for-direction */
import { SCENES, COLORS, EVENTS } from '../constants';
import { Counts } from '../customTypes';
import Field from '../objects/Field';
import Number from '../objects/Number';
import SaveGame from '../objects/SaveGame';
import Utils from '../utils';

export default class GameScene extends Phaser.Scene {
  private rows: number;
  private columns: number;
  private fieldSize: number;
  private gameArea: {
    left: number;
    top: number;
    right: number;
    bottom: number;
    width: number;
    height: number;
  };
  private fields: Array<Array<Field>>;
  private counts: Counts;
  private saveGame: SaveGame;
  public gameOver: boolean;

  constructor() {
    super({
      key: SCENES.GAME,
    });
  }

  //////////////////////////////////////////////////
  // LIFECYCLE (init, preload, create, update)    //
  //////////////////////////////////////////////////

  init(): void {}

  preload(): void {}

  create(data): void {
    this.saveGame = new SaveGame();
    // set data
    this._initData(data);
    if (data.continue) {
      this._loadData();
    }
    // background
    this.cameras.main.setBounds(0, 0, 800, 600);
    this.cameras.main.setBackgroundColor(
      Utils.colorToRGBAString(COLORS.BACKGROUND),
    );
    this._createFields();
    this._createLines();
    this._createColumnNumbers();
    this._createRowNumbers();
    this.input.mouse.disableContextMenu();
    this.scene.launch(SCENES.UI, {
      runningScene: SCENES.GAME,
      counts: this.counts,
    });
    this.saveData();
  }

  update(): void {}

  //////////////////////////////////////////////////
  // Public methods                              //
  //////////////////////////////////////////////////

  public addError() {
    this.counts.curErrors++;
    this.events.emit(EVENTS.UPDATE_ERRORS, this.counts.curErrors);
    this._checkWin();
  }

  public addFilled() {
    this.counts.curFilled++;
    this.events.emit(
      EVENTS.UPDATE_PROGRESS,
      this.counts.curFilled,
      this.counts.totalFilled,
    );
    this._checkWin();
  }

  public saveData() {
    this.saveGame.setData({
      size: { rows: this.rows, columns: this.columns },
      fields: this.fields,
      counts: this.counts,
    });
  }

  //////////////////////////////////////////////////
  // Private methods                              //
  //////////////////////////////////////////////////

  private _initData(data) {
    this.rows = data?.size?.rows || 10;
    this.columns = data?.size?.columns || 10;
    this.fieldSize = 45;
    this.gameArea = {
      left: 110,
      top: 150,
      right: 760,
      bottom: 550,
      width: 650,
      height: 400,
    };
    this.fields = [];
    this.counts = {
      totalFilled: 0,
      curFilled: 0,
      curErrors: 0,
    };
    this.gameOver = false;
  }

  private _loadData() {
    const loadedData = this.saveGame.getData();

    if (loadedData) {
      this.rows = loadedData.size.rows;
      this.columns = loadedData.size.columns;
      this._loadFields(loadedData.fieldsData);
      this.counts = loadedData.counts;
    }
  }

  private _calcFieldSize() {
    const maxHeight = this.gameArea.height / this.rows;
    const maxWidth = this.gameArea.width / this.columns;
    // take smaller size
    this.fieldSize = maxHeight > maxWidth ? maxWidth : maxHeight;
  }

  private _createFields() {
    let x = this.gameArea.left;
    let y = this.gameArea.top;
    this._calcFieldSize();

    if (!this.fields.length) {
      for (let row = 0; row < this.rows; row++) {
        this.fields.push([]);
        for (let column = 0; column < this.columns; column++) {
          const field = new Field(this, x, y, this.fieldSize, true);
          if (field.getFilled()) {
            this.counts.totalFilled++;
          }
          this.fields[row].push(field);

          x += this.fieldSize;
        }
        x = this.gameArea.left;
        y += this.fieldSize;
      }
    }
  }

  private _loadFields(fieldsData) {
    let x = this.gameArea.left;
    let y = this.gameArea.top;
    this._calcFieldSize();

    for (let row = 0; row < this.rows; row++) {
      this.fields.push([]);
      for (let column = 0; column < this.columns; column++) {
        const field = new Field(this, x, y, this.fieldSize, false);

        if (fieldsData[row][column].filled) {
          field.setFilled(true);
        }
        if (fieldsData[row][column].error) {
          field.setError(true);
        }
        if (fieldsData[row][column].solved) {
          field.setSolved(true);
        }
        this.fields[row].push(field);

        x += this.fieldSize;
      }
      x = this.gameArea.left;
      y += this.fieldSize;
    }
  }

  private _createLines() {
    const fieldsBetween = 5;
    // vertical
    const amountLinesH = this.columns / fieldsBetween - 1;
    for (let index = 1; index <= amountLinesH; index++) {
      const x = this.gameArea.left + index * fieldsBetween * this.fieldSize;
      const startY = this.gameArea.top;
      const endY = this.gameArea.top + this.rows * this.fieldSize;
      this.add
        .graphics()
        .lineStyle(1, Utils.colorToHex(COLORS.SECONDARY), 0.3)
        .lineBetween(x, startY, x, endY);
    }
    // hotizontal
    const amountLinesV = this.rows / fieldsBetween - 1;
    for (let index = 1; index <= amountLinesV; index++) {
      const y = this.gameArea.top + index * fieldsBetween * this.fieldSize;
      const startX = this.gameArea.left;
      const endX = this.gameArea.left + this.columns * this.fieldSize;
      this.add
        .graphics()
        .lineStyle(1, Utils.colorToHex(COLORS.SECONDARY), 0.3)
        .lineBetween(startX, y, endX, y);
    }
  }

  private _createRowNumbers() {
    let x = this.gameArea.left - 15;
    let y =
      this.gameArea.top +
      this.fieldSize * this.fields.length -
      this.fieldSize / 2;

    for (let row = this.fields.length - 1; row >= 0; row--) {
      let numberRow = 0;
      for (let column = this.fields[row].length - 1; column >= 0; column--) {
        if (this.fields[row][column].getFilled()) {
          // if filled: Add to number
          numberRow++;
        }
        if (
          numberRow > 0 &&
          (!this.fields[row][column].getFilled() ||
            !this.fields[row][column - 1])
        ) {
          // if empty or last one in column: Display number
          new Number(this, x, y, `${numberRow}`, {}).setOrigin(0.5);
          x -= 12;
          numberRow = 0;
        }
      }
      x = this.gameArea.left - 15;
      y -= this.fieldSize;
    }
  }

  private _createColumnNumbers() {
    let x =
      this.gameArea.left +
      this.fieldSize * this.fields[0].length -
      this.fieldSize / 2;
    let y = this.gameArea.top - 15;

    for (let column = this.fields[0].length - 1; column >= 0; column--) {
      let numberColumn = 0;
      for (let row = this.fields.length - 1; row >= 0; row--) {
        if (this.fields[row][column].getFilled()) {
          // if filled: Add to number
          numberColumn++;
        }
        if (
          numberColumn > 0 &&
          (!this.fields[row][column].getFilled() || !this.fields[row - 1])
        ) {
          // if empty or last one in row: Display number
          new Number(this, x, y, `${numberColumn}`, {}).setOrigin(0.5);
          y -= 17;
          numberColumn = 0;
        }
      }
      y = this.gameArea.top - 15;
      x -= this.fieldSize;
    }
  }

  _checkWin() {
    if (this.counts.totalFilled === this.counts.curFilled) {
      // fill all remaining empty fields
      for (let row = 0; row < this.fields.length; row++) {
        for (let column = 0; column < this.fields[row].length; column++) {
          if (this.fields[row][column].isEmpty()) {
            this.fields[row][column].onPressFieldSolveEmpty();
          }
        }
      }
      if (this.counts.curErrors === 0) {
        // animate colors if solved without errors
        this.time.addEvent({
          delay: 300,
          callback: () => {
            for (let row = 0; row < this.fields.length; row++) {
              for (let column = 0; column < this.fields[row].length; column++) {
                this.fields[row][column].changeColorRnd();
              }
            }
          },
          loop: true,
        });
      }
      this.gameOver = true;
      this.events.emit(EVENTS.GAMEOVER, this.counts.curErrors);
      return true;
    } else {
      return false;
    }
  }
}
