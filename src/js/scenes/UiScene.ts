import { SCENES, EVENTS, TEXTURES, COLORS } from '../constants';
import { Counts } from '../customTypes';
import SpriteButton from '../objects/SpriteButton';
import TextButton from '../objects/TextButton';
import Utils from '../utils';

export default class UiScene extends Phaser.Scene {
  private gameScene: Phaser.Scene;
  private progressText: Phaser.GameObjects.Text;
  private errorText: Phaser.GameObjects.Text;
  private gameOverText: Phaser.GameObjects.Text;

  constructor() {
    super({
      key: SCENES.UI,
    });
  }

  //////////////////////////////////////////////////
  // LIFECYCLE (init, preload, create, update)    //
  //////////////////////////////////////////////////

  init(): void {
    this.gameScene = this.scene.get(SCENES.GAME);
  }

  preload(): void {}

  create(data): void {
    this.progressText = null;
    this.errorText = null;
    this.gameOverText = null;
    this.createUi(data.runningScene, data.counts);
    this.setupEvents();
  }

  update(): void {}

  //////////////////////////////////////////////////
  // Private methods                              //
  //////////////////////////////////////////////////

  private createUi(runningScene: SCENES, counts: Counts) {
    // logo
    new SpriteButton(
      this,
      0,
      0,
      () => {
        this.scene.start(SCENES.MENU);
        if (runningScene) {
          this.scene.stop(runningScene);
        }
      },
      TEXTURES.LOGO,
    )
      .setOrigin(0)
      .setScale(0.3);
    if (counts) {
      // errors
      this.errorText = this.add.text(600, 0, '', {
        fontFamily: 'Nunito',
        color: '#fff',
        fontSize: '16px',
      });
      this.updateErrorText(counts.curErrors);
      // progress
      this.progressText = this.add.text(680, 0, '', {
        fontFamily: 'Nunito',
        color: '#fff',
        fontSize: '16px',
      });
      this.updateProgressText(counts.curFilled, counts.totalFilled);
    }
  }

  private setupEvents() {
    this.gameScene.events.on(
      EVENTS.UPDATE_PROGRESS,
      (curFilled: number, totalFilled: number) => {
        this.updateProgressText(curFilled, totalFilled);
      },
    );
    this.gameScene.events.on(EVENTS.UPDATE_ERRORS, (errors: number) => {
      this.updateErrorText(errors);
    });
    this.gameScene.events.on(EVENTS.GAMEOVER, (errors: number) => {
      this.showGameOverText(errors);
    });
  }

  private updateErrorText(errors: number) {
    this.errorText.setText(`Errors: ${errors}`);
  }

  private updateProgressText(curFilled: number, totalFilled: number) {
    const progress = Phaser.Math.FloorTo((curFilled / totalFilled) * 100);
    this.progressText.setText(`Progress: ${progress}%`);
  }

  private showGameOverText(errors: number) {
    if (errors === 0) {
      this.add
        .text(400, 230, 'Puzzle master', {
          fontFamily: 'Nunito',
          color: '#fff',
          fontSize: '48px',
        })
        .setOrigin(0.5);
    }
    this.add
      .text(400, 280, `You solved this puzzle with ${errors} mistakes`, {
        fontFamily: 'Nunito',
        color: '#fff',
        fontSize: '28px',
      })
      .setOrigin(0.5);

    new TextButton(
      this,
      400,
      350,
      'new game',
      {
        fontFamily: 'Nunito',
        color: Utils.colorToString(COLORS.TEXT),
        fontSize: '42px',
      },
      Utils.colorToString(COLORS.PRIMARY),
      () => {
        this.scene.start(SCENES.NEW_GAME);
      },
    ).setOrigin(0.5);
  }
}
