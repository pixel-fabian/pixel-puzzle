import { COLORS, SCENES, TEXTURES } from '../constants';
import SaveGame from '../objects/SaveGame';
import TextButton from '../objects/TextButton';
import Utils from '../utils';

export default class MenuScene extends Phaser.Scene {
  private saveGame: SaveGame;
  constructor() {
    super({
      key: SCENES.MENU,
    });
  }

  //////////////////////////////////////////////////
  // LIFECYCLE (init, preload, create, update)    //
  //////////////////////////////////////////////////

  init(): void {}

  preload(): void {}

  create(): void {
    this.saveGame = new SaveGame();
    // background
    const camera = this.cameras.add(0, 0, 800, 600);
    camera.setBackgroundColor(Utils.colorToRGBAString(COLORS.BACKGROUND));
    // logo
    this.add.image(400, 100, TEXTURES.LOGO).setOrigin(0.5);
    // buttons
    const continueButton = new TextButton(
      this,
      400,
      250,
      'continue',
      {
        fontFamily: 'Nunito',
        color: Utils.colorToString(COLORS.TEXT),
        fontSize: '42px',
      },
      Utils.colorToString(COLORS.PRIMARY),
      () => {
        this.scene.start(SCENES.GAME, { continue: true });
      },
    ).setOrigin(0.5);

    if (!this.saveGame.hasData()) {
      continueButton.setEnabled(false);
    }

    new TextButton(
      this,
      400,
      320,
      'new game',
      {
        fontFamily: 'Nunito',
        color: Utils.colorToString(COLORS.TEXT),
        fontSize: '42px',
      },
      Utils.colorToString(COLORS.PRIMARY),
      () => {
        this.scene.start(SCENES.NEW_GAME);
      },
    ).setOrigin(0.5);

    new TextButton(
      this,
      400,
      390,
      'credits',
      {
        fontFamily: 'Nunito',
        color: Utils.colorToString(COLORS.TEXT),
        fontSize: '42px',
      },
      Utils.colorToString(COLORS.PRIMARY),
      () => {
        this.scene.start(SCENES.CREDITS);
      },
    ).setOrigin(0.5);
  }

  update(): void {}

  //////////////////////////////////////////////////
  // Private methods                              //
  //////////////////////////////////////////////////
}
