import { COLORS, SCENES } from '../constants';
import Utils from '../utils';

export default class CretidsScene extends Phaser.Scene {
  constructor() {
    super({
      key: SCENES.CREDITS,
    });
  }

  //////////////////////////////////////////////////
  // LIFECYCLE (init, preload, create, update)    //
  //////////////////////////////////////////////////

  init(): void {
    this.scene.launch(SCENES.UI, { runningScene: SCENES.CREDITS });
  }

  preload(): void {}

  create(): void {
    const screenCenterX = this.scale.width / 2;
    this.add
      .text(screenCenterX, 120, 'credits', {
        fontFamily: 'Nunito',
        color: Utils.colorToString(COLORS.TEXT),
        fontSize: '64px',
      })
      .setOrigin(0.5);

    this.add
      .text(screenCenterX, 250, 'A Game by: pixel-fabian (GPL 3.0)', {
        fontFamily: 'Nunito',
        color: '#fff',
        fontSize: '28px',
      })
      .setOrigin(0.5);
    this.add
      .text(screenCenterX, 300, 'Assets: pixel-fabian (CC-BY-SA 4.0)', {
        fontFamily: 'Nunito',
        color: '#fff',
        fontSize: '28px',
      })
      .setOrigin(0.5);
    this.add
      .text(
        screenCenterX,
        350,
        'Font: Nunito by Vernon Adams (SIL Open Font License)',
        {
          fontFamily: 'Nunito',
          color: '#fff',
          fontSize: '28px',
        },
      )
      .setOrigin(0.5);
    this.add
      .text(screenCenterX, 400, 'Game engine: Phaser 3 by PhotonStorm', {
        fontFamily: 'Nunito',
        color: '#fff',
        fontSize: '28px',
      })
      .setOrigin(0.5);
  }

  update(): void {}

  //////////////////////////////////////////////////
  // Private methods                              //
  //////////////////////////////////////////////////
}
