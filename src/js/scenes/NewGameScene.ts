import { COLORS, SCENES } from '../constants';
import TextButton from '../objects/TextButton';
import Utils from '../utils';

export default class NewGameScene extends Phaser.Scene {
  private sizes = [
    // { rows: 2, columns: 2 },
    { rows: 5, columns: 10 },
    { rows: 10, columns: 10 },
    { rows: 10, columns: 15 },
  ];

  constructor() {
    super({
      key: SCENES.NEW_GAME,
    });
  }

  //////////////////////////////////////////////////
  // LIFECYCLE (init, preload, create, update)    //
  //////////////////////////////////////////////////

  init(): void {
    this.scene.launch(SCENES.UI, { runningScene: SCENES.NEW_GAME });
  }

  preload(): void {}

  create(): void {
    // background
    const camera = this.cameras.add(0, 0, 800, 600);
    camera.setBackgroundColor(Utils.colorToRGBAString(COLORS.BACKGROUND));
    // text
    this.add
      .text(400, 120, 'new game', {
        fontFamily: 'Nunito',
        color: Utils.colorToString(COLORS.TEXT),
        fontSize: '64px',
      })
      .setOrigin(0.5);

    let y = 220;
    this.sizes.forEach((element) => {
      new TextButton(
        this,
        400,
        y,
        `${element.rows} x ${element.columns}`,
        {
          fontFamily: 'Nunito',
          color: Utils.colorToString(COLORS.TEXT),
          fontSize: '32px',
        },
        Utils.colorToString(COLORS.PRIMARY),
        (size = element) => {
          this.scene.start(SCENES.GAME, { continue: false, size: size });
        },
      ).setOrigin(0.5);
      y += 50;
    });
  }

  update(): void {}

  //////////////////////////////////////////////////
  // Private methods                              //
  //////////////////////////////////////////////////
}
