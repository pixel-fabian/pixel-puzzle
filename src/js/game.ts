import 'phaser';
import LoadScene from './scenes/LoadScene';
import MenuScene from './scenes/MenuScene';
import GameScene from './scenes/GameScene';
import NewGameScene from './scenes/NewGameScene';
import UiScene from './scenes/UiScene';
import CreditsScene from './scenes/CreditsScene';

const config: Phaser.Types.Core.GameConfig = {
  type: Phaser.AUTO, // WebGL if available
  title: 'Blueprint',
  width: 800,
  height: 600,
  scale: {
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
    max: {
      width: 800,
      height: 600,
    },
  },
  parent: 'game',
  scene: [LoadScene, MenuScene, GameScene, NewGameScene, UiScene, CreditsScene],
};

window.onload = () => {
  new Phaser.Game(config);
};
